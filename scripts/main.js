"use strict"

const list = ["Київ", ["Borispol", "Irpin"], "Харків", "Львів", "Чернігів", "Одеса", "Чернівці"];

// Rekursia forEach:

// const divContainer = document.querySelector('.container');


// const getList = (arr, parent = document.body) => {

//   const ul = document.createElement('ul');
//   parent.append(ul);

//   arr.forEach(element => {
//     const li = document.createElement('li');
//     if (Array.isArray(element)) {
//       li.append(getList(element, arr[element - 1]))
//     } else {
//       li.innerHTML = element;
//       li.className = 'list-item';
//     }
//     ul.append(li);
//   })
//   return ul;
// };



// Rekursia map:

const divContainer = document.querySelector('.container');


const getList = (arr, parent = document.body) => {

  const ul = document.createElement('ul');
  parent.append(ul);

  const newArr = arr.map((el) => {
    const li = document.createElement('li');

    if (Array.isArray(el)) {
    
      ul.append(getList(el));

    } else {
    
    li.innerHTML = el;
    li.className = "list-container";
    ul.append(li);
    }
  });
  return ul;
};

getList(list, divContainer)
